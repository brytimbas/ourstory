<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '_bc');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'HvxX[gD>t=/7c>z.uTXW@)eFILFJ]7V%[ ~}o9 W9*k=,&l/h:vI?|5GJwm9Ce^2');
define('SECURE_AUTH_KEY',  'mSK.0L4T34t4<3=fqxfJfm`T_qlgDlboE(ZKl4QXq&O<lhx|o8x*+C~L.%-i !|I');
define('LOGGED_IN_KEY',    '|0g0A[D$N7vM3Wc0HB-#r &0xKmlaX*#!iGd&h@grj)9Rv5w0?SSY?7hn.ce!~25');
define('NONCE_KEY',        '?]oN&?k2^zm?O6E;`|socMkT@P!SUMe5^<*GSV~HVw!]uTW|-:cU~D{iQesP>ShL');
define('AUTH_SALT',        '7 oYVBM4-CWCN/$+EO:M-:ep1>E_p(B @&Kz#1L555mTD(]bllzsvtJL-$kEWAeF');
define('SECURE_AUTH_SALT', 'l!/$)Utx8I$]}?%4HP90I8HOV(v6![`8I9!#.7^N3<GE6!3^.@LOXf?Lde?_zQ&$');
define('LOGGED_IN_SALT',   '-kukb8#*m{xk7Mol?^KW51t|a yKn`tJd|R x2HrN[wW&HiC H>g49#v)wPbEQrQ');
define('NONCE_SALT',       '0PXjL/>V>eE{SQpoCK-Jxvx/i@p}_#5{<vLSc_;$.IaLDI%b%fLfj0~X:d)=()p~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
